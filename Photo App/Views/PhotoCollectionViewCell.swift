//
//  PhotoCollectionViewCell.swift
//  Photo App
//
//  Created by Alexander Danilyak on 16/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import UIKit
import Photos

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var loadingProgressView: UIProgressView?
    @IBOutlet weak var loadingLabel: UILabel?
    
    //MARK: Properties
    
    var photoImage: UIImage! {
        didSet {
            photoImageView.image = photoImage
        }
    }
    
    var asset: PHAsset!
    
    var targetSize: CGSize {
        let scale = UIScreen.main.scale
        return CGSize(width: photoImageView.bounds.width * scale,
                      height: photoImageView.bounds.height * scale)
    }
    
    
    //MARK: Methods
    
    func requestPhoto() {
        let requestItem = DispatchWorkItem {
            let imageRequestOption = PHImageRequestOptions()
            imageRequestOption.isNetworkAccessAllowed = true
            imageRequestOption.deliveryMode = .highQualityFormat
            imageRequestOption.progressHandler = { progress, _, _, _ in
                DispatchQueue.main.async {
                    self.loadingProgressView?.progress = Float(progress)
                }
            }
            
            PHImageManager.default().requestImage(for: self.asset,
                                                  targetSize: self.targetSize,
                                                  contentMode: .aspectFit,
                                                  options: imageRequestOption,
                                                  resultHandler: { (resultImage, info) in
                                                    if resultImage != nil { self.photoImage = resultImage! }
                                                    self.loadingProgressView?.isHidden = true
                                                    self.loadingLabel?.isHidden = true
            })
        }
        
        DispatchQueue.main.async(execute: requestItem)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = nil
        self.loadingProgressView?.isHidden = false
        self.loadingLabel?.isHidden = false
    }
}
