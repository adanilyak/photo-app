//
//  AlbumCollectionViewCell.swift
//  Photo App
//
//  Created by Alexander Danilyak on 14/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import UIKit
import Photos

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lastPhotoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var photoCountLabel: UILabel!
    
    
    //MARK: Properties
    
    var lastPhotoThumbnailImage: UIImage! {
        didSet {
            lastPhotoImageView.image = lastPhotoThumbnailImage
        }
    }
    
    var targetSize: CGSize {
        let scale = UIScreen.main.scale
        return CGSize(width: lastPhotoImageView.bounds.width * scale,
                      height: lastPhotoImageView.bounds.height * scale)
    }
    
    var assets: PHFetchResult<PHAsset>!
    
    var assetCollection: PHAssetCollection! {
        didSet {
            titleLabel.text = assetCollection.localizedTitle
            requestLastPhoto()
        }
    }
    
    
    //MARK: Methods
    
    func requestLastPhoto() {
        let setCountLabelAndRequestPhotoItem = DispatchWorkItem {
            self.photoCountLabel.text = String(self.assets.count)
            
            let imageRequestOption = PHImageRequestOptions()
            imageRequestOption.isNetworkAccessAllowed = true
            imageRequestOption.deliveryMode = .opportunistic
            
            guard let lastPhotoAsset = self.assets.firstObject else { return }
            PHImageManager.default().requestImage(for: lastPhotoAsset,
                                                  targetSize: self.targetSize,
                                                  contentMode: .aspectFill,
                                                  options: imageRequestOption,
                                                  resultHandler: { (resultImage, info) in
                                                    self.lastPhotoThumbnailImage = resultImage
            })
        }
        
        let fetchItem = DispatchWorkItem {
            self.assets = PHAsset.fetchAssets(in: self.assetCollection, options: onlyPhotosOptions)
            DispatchQueue.main.async(execute: setCountLabelAndRequestPhotoItem)
        }
        
        fetchQueue.async(execute: fetchItem)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lastPhotoImageView.image = nil
    }
}
