//
//  AlbumsHeaderReusableView.swift
//  Photo App
//
//  Created by Alexander Danilyak on 15/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import UIKit

class AlbumsHeaderReusableView: UICollectionReusableView {
    @IBOutlet weak var headerLabel: UILabel!
}
