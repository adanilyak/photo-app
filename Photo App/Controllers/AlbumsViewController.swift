//
//  AlbumsViewController.swift
//  Photo App
//
//  Created by Alexander Danilyak on 14/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import UIKit
import Photos

class AlbumsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noAccessLabel: UILabel!
    @IBOutlet weak var openSettingsButton: UIButton!
    
    //MARK: Type for working with sections,identifiers for cells and segues
    
    enum Section: Int {
        case smartAlbums
        case userAlbums
        
        static let count = 2
    }
    
    enum Identifiers {
        static let cellIdentifier = "albumCell"
        static let headerIdentifier = "albumsHeader"
    }
    
    let openAlbumSegue = "openAlbum"
    
    
    //MARK: Properties
    
    var albumsManager: AlbumsManager!
    let sectionTitles = ["Smart Albums", "User Albums"]
    
    enum AlbumsCollectionParams {
        static let itemsPerRow: CGFloat = 2
        static let itemMargins: CGFloat = 10.0
        static let heightOfLabelsBlock: CGFloat = 54.0
        static let sectionInsets = UIEdgeInsets(top: AlbumsCollectionParams.itemMargins,
                                                left: AlbumsCollectionParams.itemMargins,
                                                bottom: AlbumsCollectionParams.itemMargins,
                                                right: AlbumsCollectionParams.itemMargins)
    }
    
    var canOpenSettings: Bool = {
        guard let settingsURL = URL(string: UIApplicationOpenSettingsURLString) else { return false }
        return UIApplication.shared.canOpenURL(settingsURL)
    }()
    
    
    //MARK: UIViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setAlbumsCollectionHidden(to: true, animated: false)
        setNoAccessInformationHidden(to: true, animated: false)
        
        albumsManager = AlbumsManager()
        albumsManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkAccessToPhotos()
    }
    
    func checkAccessToPhotos() {
        let authorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch authorizationStatus {
        case .authorized:
            if !albumsManager.isFetchResultsReady { albumsManager.fetchAllAlbums() }
            break
        case .denied:
            DispatchQueue.main.async {
                self.setNoAccessInformationHidden(to: false, animated: true)
            }
            break
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { status in
                if status != .notDetermined { self.checkAccessToPhotos() }
            }
            break
        case .restricted:
            // very special case, access is not granted and we cann't ask user for such permisions
            // do nothing
            break
        }
    }
    
    
    //MARK: UI Methods
    
    func setAlbumsCollectionHidden(to hidden: Bool, animated: Bool) {
        let duration = animated ? 0.5 : 0.0
        UIView.animate(withDuration: duration) {
            self.collectionView.alpha = hidden ? 0.0 : 1.0
        }
    }
    
    func setNoAccessInformationHidden(to hidden: Bool, animated: Bool) {
        let duration = animated ? 0.5 : 0.0
        UIView.animate(withDuration: duration) {
            self.noAccessLabel.alpha = hidden ? 0.0 : 1.0
            self.openSettingsButton.alpha = (hidden && self.canOpenSettings) ? 0.0 : 1.0
        }
    }
    
    
    //MARK: Buttons
    
    @IBAction func onOpenSettings(_ sender: AnyObject) {
        guard let settingsURL = URL(string: UIApplicationOpenSettingsURLString) else { return }
        UIApplication.shared.openURL(settingsURL)
    }
    
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == openAlbumSegue {
            guard let destination = segue.destination as? AlbumViewController else { fatalError("unexpected destination") }
            let selectedCellIndexPath = sender as! IndexPath

            let assetCollection: PHAssetCollection
            
            switch Section.init(rawValue: selectedCellIndexPath.section)! {
            case .smartAlbums:
                assetCollection = albumsManager.onlyPhotoSmartAlbums[selectedCellIndexPath.row]
                break
            case .userAlbums:
                assetCollection = albumsManager.userAlbumsWithAssets[selectedCellIndexPath.row]
                break
            }
            
            destination.albumManager = AlbumManager(with: assetCollection, and: destination)
            destination.title = assetCollection.localizedTitle
        }
    }
    
}

extension AlbumsViewController: AlbumsManagerProtocol {
    func didFinishFetch() {
        collectionView.reloadData()
        setAlbumsCollectionHidden(to: false, animated: true)
    }
}

extension AlbumsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return albumsManager.isFetchResultsReady ? Section.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
            case .smartAlbums: return albumsManager.onlyPhotoSmartAlbums.count
            case .userAlbums: return albumsManager.userAlbumsWithAssets.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AlbumCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.cellIdentifier, for: indexPath) as! AlbumCollectionViewCell
        
        let assetCollection: PHAssetCollection
        
        switch Section.init(rawValue: indexPath.section)! {
        case .smartAlbums:
            assetCollection = albumsManager.onlyPhotoSmartAlbums[indexPath.row]
            break
        case .userAlbums:
            assetCollection = albumsManager.userAlbumsWithAssets[indexPath.row]
            break
        }
        
        cell.assetCollection = assetCollection
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let albumsHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: Identifiers.headerIdentifier, for: indexPath) as? AlbumsHeaderReusableView else { return UICollectionReusableView() }
        
        albumsHeaderView.headerLabel.text = sectionTitles[indexPath.section]
        return albumsHeaderView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: openAlbumSegue, sender: indexPath)
    }
}

extension AlbumsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let allMargins = (AlbumsCollectionParams.itemsPerRow + 1) * AlbumsCollectionParams.itemMargins
        let itemWidth = (view.frame.width - allMargins) / AlbumsCollectionParams.itemsPerRow
        
        return CGSize(width: itemWidth, height: itemWidth + AlbumsCollectionParams.heightOfLabelsBlock)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return AlbumsCollectionParams.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return AlbumsCollectionParams.sectionInsets.left
    }
}
