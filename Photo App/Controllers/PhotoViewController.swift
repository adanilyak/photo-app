//
//  PhotoViewController.swift
//  Photo App
//
//  Created by Alexander Danilyak on 16/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import UIKit
import Photos

class PhotoViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: identifiers for cells and segues
    
    let photoCellIdentifier = "singlePhotoCell"
    
    
    //MARK: Properties
    
    enum PhotoCollectionParams {
        static let itemsPerRow: CGFloat = 1
        static let sectionInsets = UIEdgeInsets.zero
    }
    
    var albumManager: AlbumManager!
    
    var currentPhoto: Int! {
        didSet {
            title = "\(currentPhoto! + 1) of \(photoCount)"
        }
    }
    var photoCount: Int {
        return albumManager.isFetchResultsReady ? albumManager.assets.count : 0
    }
    
    var isFirstDidLayoutSubviews = true
    
    //MARK: UIViewController
    
    override func viewDidLayoutSubviews() {
        if isFirstDidLayoutSubviews {
            collectionView.scrollToItem(at: IndexPath(row: currentPhoto, section: 0), at: .right, animated: false)
            isFirstDidLayoutSubviews = false
        }
    }
}

extension PhotoViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return albumManager.isFetchResultsReady ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albumManager.assets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: photoCellIdentifier, for: indexPath) as! PhotoCollectionViewCell
        cell.asset = albumManager.assets.object(at: indexPath.row)
        cell.requestPhoto()
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let currentIndexPath = collectionView.indexPathForItem(at: scrollView.contentOffset) else { return }
        currentPhoto = currentIndexPath.row
    }
}

extension PhotoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return UIScreen.main.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return PhotoCollectionParams.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return PhotoCollectionParams.sectionInsets.left
    }
}
