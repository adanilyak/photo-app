//
//  AlbumViewController.swift
//  Photo App
//
//  Created by Alexander Danilyak on 16/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import UIKit
import Photos

class AlbumViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noPhotoLabel: UILabel!
    
    
    //MARK: identifiers for cells and segues
    
    let photoCellIdentifier = "photoCell"
    let openPhotoSegue = "openPhoto"
    
    
    //MARK: Properties
    
    enum AlbumCollectionParams {
        static let itemsPerRow: CGFloat = 4
        static let itemMargins: CGFloat = 1.0
        static let sectionInsets = UIEdgeInsets.zero
    }
    
    var albumManager: AlbumManager!
    
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNoPhotoInformationHidden(to: true, animated: false)
    }
    
    
    //MARK: UI Methods
    
    func setNoPhotoInformationHidden(to hidden: Bool, animated: Bool) {
        let duration = animated ? 0.5 : 0.0
        UIView.animate(withDuration: duration) {
            self.noPhotoLabel.alpha = hidden ? 0.0 : 1.0
        }
    }
    
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == openPhotoSegue {
            guard let destination = segue.destination as? PhotoViewController else { fatalError("unexpected destination") }
            let selectedCellIndexPath = sender as! IndexPath
            
            destination.albumManager = albumManager
            destination.currentPhoto = selectedCellIndexPath.row
        }
    }
}

extension AlbumViewController: AlbumManagerProtocol {
    func didFinishFetch() {
        if albumManager.assets.count == 0 {
            setNoPhotoInformationHidden(to: false, animated: true)
        } else {
            collectionView.reloadData()
        }
    }
}

extension AlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return albumManager.isFetchResultsReady ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albumManager.assets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: photoCellIdentifier, for: indexPath) as! PhotoCollectionViewCell
        cell.asset = albumManager.assets.object(at: indexPath.row)
        cell.requestPhoto()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: openPhotoSegue, sender: indexPath)
    }
}

extension AlbumViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let allMargins = (AlbumCollectionParams.itemsPerRow - 1) * AlbumCollectionParams.itemMargins
        let itemWidth = (view.frame.width - allMargins) / AlbumCollectionParams.itemsPerRow
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return AlbumCollectionParams.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return AlbumCollectionParams.itemMargins
    }
}
