//
//  FetchOptions.swift
//  Photo App
//
//  Created by Alexander Danilyak on 16/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import Foundation
import Photos

let onlyPhotosOptions: PHFetchOptions = {
    let options = PHFetchOptions()
    options.predicate = NSPredicate(format: "mediaType==%d", PHAssetMediaType.image.rawValue)
    options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
    return options
}()
