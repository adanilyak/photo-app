//
//  FetchQueue.swift
//  Photo App
//
//  Created by Alexander Danilyak on 15/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import Foundation

let fetchQueueLabel = "com.photoapp.fetch"
let fetchQueue = DispatchQueue(label: fetchQueueLabel,
                               qos: DispatchQoS.background,
                               attributes: DispatchQueue.Attributes.concurrent,
                               autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit,
                               target: nil)
