//
//  AlbumManager.swift
//  Photo App
//
//  Created by Alexander Danilyak on 16/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import Foundation
import Photos

protocol AlbumManagerProtocol {
    func didFinishFetch()
}

class AlbumManager {
    
    //MARK: Properties
    
    var assetCollection: PHAssetCollection!
    var assets: PHFetchResult<PHAsset>!
    
    var delegate: AlbumManagerProtocol?
    
    var isFetchResultsReady: Bool {
        return assets != nil
    }
    
    
    //MARK: Init
    
    init(with assetCollection: PHAssetCollection, and delegate: AlbumManagerProtocol) {
        self.assetCollection = assetCollection
        self.delegate = delegate
        
        fetchAssetCollection()
    }
    
    
    //MARK: Methods
    
    func fetchAssetCollection() {
        let fetchItem = DispatchWorkItem {
            self.assets = PHAsset.fetchAssets(in: self.assetCollection, options: onlyPhotosOptions)
            DispatchQueue.main.async { self.delegate?.didFinishFetch() }
        }
        
        fetchQueue.async(execute: fetchItem)
    }
}
