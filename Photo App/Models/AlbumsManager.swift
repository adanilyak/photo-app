//
//  AlbumsManager.swift
//  Photo App
//
//  Created by Alexander Danilyak on 14/10/2016.
//  Copyright © 2016 adanilyak. All rights reserved.
//

import Foundation
import Photos

protocol AlbumsManagerProtocol {
    func didFinishFetch()
}

class AlbumsManager {
    
    //MARK: Properties
    
    var smartAlbums: PHFetchResult<PHAssetCollection>!
    var onlyPhotoSmartAlbums: [PHAssetCollection]! = []
    
    var allUserAlbums: PHFetchResult<PHCollection>!
    var userAlbumsWithAssets: [PHAssetCollection]! = []
    
    var delegate: AlbumsManagerProtocol?
    
    var isFetchResultsReady: Bool = false
    
    
    //MARK: Methods
    
    func fetchAllAlbums() {
        let fetchItem = DispatchWorkItem {
            self.smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: nil)
            self.smartAlbums.enumerateObjects({ (smartAlbum, index, stop) in
                let noPhotoSubTypes: [PHAssetCollectionSubtype] = [.smartAlbumVideos, .smartAlbumTimelapses, .smartAlbumSlomoVideos, .smartAlbumAllHidden]
                if !noPhotoSubTypes.contains(smartAlbum.assetCollectionSubtype) { self.onlyPhotoSmartAlbums.append(smartAlbum) }
            })
            
            self.allUserAlbums = PHCollectionList.fetchTopLevelUserCollections(with: nil)
            self.allUserAlbums.enumerateObjects({ (collection, index, stop) in
                if collection.canContainAssets { self.userAlbumsWithAssets.append(collection as! PHAssetCollection) }
            })
            
            self.isFetchResultsReady = true
            
            DispatchQueue.main.async { self.delegate?.didFinishFetch() }
        }
        
        fetchQueue.async(execute: fetchItem)
    }
}
